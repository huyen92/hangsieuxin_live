<?php
define('TLT_BLOG_DB_VERSION', '5.1.3');
define('TLT_BLOG_EDITION', 'PRO');
define('TLT_BLOG_VERSION', '5.1.7');
define('TLT_BLOG_DROP_DB', false);

class ControllerExtensionTltBlogTltBlogSettings extends Controller {
	private $error = array();

	public function index()
    {
		$this->load->language('extension/tltblog/tltblog_settings');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		$this->load->model('localisation/language');
		$this->load->model('extension/tltblog/url_alias');

        $data['languages'] = $languages = $this->model_localisation_language->getLanguages();

        if ($this->config->get('tltblog_db_version') !== TLT_BLOG_VERSION) $this->upgrade();

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if ($this->request->post['tltblog_facebook_page']) {
				$this->request->post['tltblog_facebook_page'] = preg_replace(array('/\s/', '/\//'), '', $this->request->post['tltblog_facebook_page']);
			}

			$request_post = $this->request->post;

            if ($this->request->post['tltblog_status']) {
                $request_post['tltblog_tltblog_settings_status'] = '1';
            } else {
                $request_post['tltblog_tltblog_settings_status'] = '0';
            }

            $request_post['tltblog_tltblog_status'] = $this->config->has('tltblog_tltblog_status') ? $this->config->get('tltblog_tltblog_status') : '0';
            $request_post['tltblog_tlttag_status'] = $this->config->has('tltblog_tlttag_status') ? $this->config->get('tltblog_tlttag_status') : '0';

            $request_post['tltblog_db_version'] = TLT_BLOG_DB_VERSION;
            $request_post['tltblog_edition'] = TLT_BLOG_EDITION;
            $request_post['tltblog_version'] = TLT_BLOG_VERSION;

			$this->model_setting_setting->editSetting('tltblog', $request_post);

			$this->model_extension_tltblog_url_alias->deleteUrlAlias('tltpath=');

			foreach ($this->request->post['tltblog_path'] as $language_id => $value) {
                $this->model_extension_tltblog_url_alias->saveUrlAlias($this->db->escape($value), 'tltpath=' . $this->db->escape($value), $language_id);
            }

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=tltblog', true));
		}

        $language_keys = array(
            'heading_title',
            'text_edit',
            'text_enabled',
            'text_disabled',
            'text_yes',
            'text_no',
            'text_summary',
            'text_large_image',
            'entry_path',
            'entry_path_title',
            'entry_show_path',
            'entry_num_columns',
            'entry_blogs_page',
            'entry_show_date',
            'entry_date_format',
            'entry_show_image',
            'entry_width',
            'entry_height',
            'entry_seo',
            'entry_schemaorg',
            'entry_schemaorg_image',
            'entry_status',
            'entry_meta_title',
            'entry_meta_description',
            'entry_meta_keyword',
            'entry_menu',
            'entry_twitter',
            'entry_twitter_card',
            'entry_twitter_name',
            'entry_facebook',
            'entry_facebook_name',
            'entry_facebook_appid',
            'entry_resize_image',
            'help_facebook_page',
            'help_path',
            'help_path_title',
            'help_show_path',
            'help_blogs_page',
            'help_show_image',
            'help_num_columns',
            'help_date_format',
            'help_seo',
            'help_schemaorg_image',
            'help_status',
            'help_meta',
            'help_menu',
            'help_twitter_status',
            'help_facebook_status',
            'help_resize_image',
            'entry_facebook_page',
            'placeholder_blogs_page',
            'placeholder_username',
            'button_save',
            'button_cancel',
            'tab_general',
            'tab_design',
            'tab_structured_data',
            'error_seo_disabled'
        );

        foreach ($language_keys as $key) {
            $data[$key] = $this->language->get($key);
        }

        $data['version'] = 'V: ' . TLT_BLOG_VERSION . ' DB: ' . TLT_BLOG_DB_VERSION . ' E: ' . TLT_BLOG_EDITION;

		if (!property_exists('Document', 'tlt_metatags')) {
			$data['error_library'] = $this->language->get('error_library');
		} else {
			$data['error_library'] = '';
		}
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['tltblog_path'])) {
			$data['error_path'] = $this->error['tltblog_path'];
		} else {
			$data['error_path'] = array();
		}

		if (isset($this->error['tltblog_path_title'])) {
			$data['error_path_title'] = $this->error['tltblog_path_title'];
		} else {
			$data['error_path_title'] = array();
		}

		if (isset($this->error['tltblog_date_format'])) {
			$data['error_date_format'] = $this->error['tltblog_date_format'];
		} else {
			$data['error_date_format'] = '';
		}

		if (isset($this->error['tltblog_width'])) {
			$data['error_width'] = $this->error['tltblog_width'];
		} else {
			$data['error_width'] = '';
		}

		if (isset($this->error['tltblog_height'])) {
			$data['error_height'] = $this->error['tltblog_height'];
		} else {
			$data['error_height'] = '';
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['twitter_name'])) {
			$data['error_twitter_name'] = $this->error['twitter_name'];
		} else {
			$data['error_twitter_name'] = array();
		}
		
		if (isset($this->error['facebook_name'])) {
			$data['error_facebook_name'] = $this->error['facebook_name'];
		} else {
			$data['error_facebook_name'] = array();
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=tltblog', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/tltblog/tltblog_settings', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/tltblog/tltblog_settings', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=tltblog', true);

		if (isset($this->request->post['tltblog_status'])) {
			$data['tltblog_status'] = $this->request->post['tltblog_status'];
		} else {
			$data['tltblog_status'] = $this->config->get('tltblog_status');
		}

        if (isset($this->request->post['tltblog_path'])) {
			$data['tltblog_path'] = $this->request->post['tltblog_path'];
		} elseif ($this->config->has('tltblog_path')) {
			$data['tltblog_path'] = $this->config->get('tltblog_path');
		} else {
			$data['tltblog_path'] = array();
		}

		if (isset($this->request->post['tltblog_path_title'])) {
			$data['tltblog_path_title'] = $this->request->post['tltblog_path_title'];
		} elseif ($this->config->has('tltblog_path_title')) {
			$data['tltblog_path_title'] = $this->config->get('tltblog_path_title');
		} else {
			$data['tltblog_path_title'] = array();
		}

		if (isset($this->request->post['tltblog_meta'])) {
			$data['tltblog_meta'] = $this->request->post['tltblog_meta'];
		} elseif ($this->config->has('tltblog_meta')) {
			$data['tltblog_meta'] = $this->config->get('tltblog_meta');
		} else {
			$data['tltblog_meta'] = array();
		}

		if (isset($this->request->post['tltblog_show_path'])) {
			$data['tltblog_show_path'] = $this->request->post['tltblog_show_path'];
		} elseif ($this->config->has('tltblog_show_path')) {
			$data['tltblog_show_path'] = $this->config->get('tltblog_show_path');
		} else {
			$data['tltblog_show_path'] = '1';
		}

		if (isset($this->request->post['tltblog_num_columns'])) {
			$data['tltblog_num_columns'] = $this->request->post['tltblog_num_columns'];
		} elseif ($this->config->has('tltblog_num_columns')) {
			$data['tltblog_num_columns'] = $this->config->get('tltblog_num_columns');
		} else {
			$data['tltblog_num_columns'] = '1';
		}

		if (isset($this->request->post['tltblog_blogs_page'])) {
			$data['tltblog_blogs_page'] = $this->request->post['tltblog_blogs_page'];
		} elseif ($this->config->has('tltblog_blogs_page')) {
			$data['tltblog_blogs_page'] = (int)$this->config->get('tltblog_blogs_page');
		} else {
			$data['tltblog_blogs_page'] = '';
		}

		if (isset($this->request->post['tltblog_show_date'])) {
			$data['tltblog_show_image'] = $this->request->post['tltblog_show_date'];
		} elseif ($this->config->has('tltblog_show_date')) {
			$data['tltblog_show_date'] = $this->config->get('tltblog_show_date');
		} else {
			$data['tltblog_show_date'] = '0';
		}

		if (isset($this->request->post['tltblog_date_format'])) {
			$data['tltblog_date_format'] = $this->request->post['tltblog_date_format'];
		} elseif ($this->config->has('tltblog_date_format')) {
			$data['tltblog_date_format'] = $this->config->get('tltblog_date_format');
		} else {
			$data['tltblog_date_format'] = 'd.m.Y H:i';
		}

		if (isset($this->request->post['tltblog_show_image'])) {
			$data['tltblog_show_image'] = $this->request->post['tltblog_show_image'];
		} elseif ($this->config->has('tltblog_show_image')) {
			$data['tltblog_show_image'] = $this->config->get('tltblog_show_image');
		} else {
			$data['tltblog_show_image'] = '1';
		}

		if (isset($this->request->post['tltblog_width'])) {
			$data['tltblog_width'] = $this->request->post['tltblog_width'];
		} elseif ($this->config->has('tltblog_width')) {
			$data['tltblog_width'] = (int)$this->config->get('tltblog_width');
		} else {
			$data['tltblog_width'] = 200;
		}

		if (isset($this->request->post['tltblog_height'])) {
			$data['tltblog_height'] = $this->request->post['tltblog_height'];
		} elseif ($this->config->has('tltblog_height')) {
			$data['tltblog_height'] = (int)$this->config->get('tltblog_height');
		} else {
			$data['tltblog_height'] = 200;
		}

		if (isset($this->request->post['tltblog_seo'])) {
			$data['tltblog_seo'] = $this->request->post['tltblog_seo'];
		} elseif ($this->config->has('tltblog_seo')) {
			$data['tltblog_seo'] = $this->config->get('tltblog_seo');
		} else {
			$data['tltblog_seo'] = '0';
		} 

		// If you have non-standard SEO module, which doesn't use config_seo_url setting simple replace this if ... else contruction with 
		$data['tltblog_global_seo_off'] = false;
		// <--begin-->
		// if ($this->config->get('config_seo_url')) {
		// 	$data['global_seo_off'] = false;
		// } else {
		// 	$data['global_seo_off'] = true;
		// } 
		// <---end--->

		if (isset($this->request->post['tltblog_schemaorg'])) {
			$data['tltblog_schemaorg'] = $this->request->post['tltblog_schemaorg'];
		} else {
			$data['tltblog_schemaorg'] = $this->config->get('tltblog_schemaorg');
		}

		if (isset($this->request->post['tltblog_schemaorg_image'])) {
			$data['tltblog_schemaorg_image'] = $this->request->post['tltblog_schemaorg_image'];
		} else {
			$data['tltblog_schemaorg_image'] = $this->config->get('tltblog_schemaorg_image');
		}

		if (isset($this->request->post['tltblog_menu'])) {
			$data['tltblog_menu'] = $this->request->post['tltblog_menu'];
		} elseif ($this->config->has('tltblog_menu')) {
			$data['tltblog_menu'] = $this->config->get('tltblog_menu');
		} else {
			$data['tltblog_menu'] = '0';
		} 

		if (isset($this->request->post['tltblog_twitter'])) {
			$data['tltblog_twitter'] = $this->request->post['tltblog_twitter'];
		} else {
			$data['tltblog_twitter'] = $this->config->get('tltblog_twitter');
		}

		if (isset($this->request->post['tltblog_twitter_card'])) {
			$data['tltblog_twitter_card'] = $this->request->post['tltblog_twitter_card'];
		} else {
			$data['tltblog_twitter_card'] = $this->config->get('tltblog_twitter_card');
		}

		if (isset($this->request->post['tltblog_twitter_name'])) {
			$data['tltblog_twitter_name'] = $this->request->post['tltblog_twitter_name'];
		} elseif ($this->config->has('tltblog_twitter_name')) {
			$data['tltblog_twitter_name'] = $this->config->get('tltblog_twitter_name');
		} else {
			$data['tltblog_twitter_name'] = '';
		}

		if (isset($this->request->post['tltblog_facebook'])) {
			$data['tltblog_facebook'] = $this->request->post['tltblog_facebook'];
		} else {
			$data['tltblog_facebook'] = $this->config->get('tltblog_facebook');
		}
		
		if (isset($this->request->post['tltblog_facebook_name'])) {
			$data['tltblog_facebook_name'] = $this->request->post['tltblog_facebook_name'];
		} elseif ($this->config->has('tltblog_facebook_name')) {
			$data['tltblog_facebook_name'] = $this->config->get('tltblog_facebook_name');
		} else {
			$data['tltblog_facebook_name'] = '';
		}

		if (isset($this->request->post['tltblog_facebook_page'])) {
			$data['tltblog_facebook_page'] = preg_replace(array('/\s/', '/\//'), '', $this->request->post['tltblog_facebook_page']);
		} elseif ($this->config->has('tltblog_facebook_page')) {
			$data['tltblog_facebook_page'] = $this->config->get('tltblog_facebook_page');
		} else {
			$data['tltblog_facebook_page'] = '';
		}
		
		if (isset($this->request->post['tltblog_facebook_appid'])) {
			$data['tltblog_facebook_appid'] = $this->request->post['tltblog_facebook_appid'];
		} elseif ($this->config->has('tltblog_facebook_appid')) {
			$data['tltblog_facebook_appid'] = $this->config->get('tltblog_facebook_appid');
		} else {
			$data['tltblog_facebook_appid'] = '';
		}
		
		if (isset($this->request->post['tltblog_resize_image'])) {
			$data['tltblog_resize_image'] = $this->request->post['tltblog_resize_image'];
		} else {
			$data['tltblog_resize_image'] = $this->config->get('tltblog_resize_image');
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/tltblog/tltblog_settings', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/tltblog/tltblog_settings')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

        $languages = $this->model_localisation_language->getLanguages();

        foreach ($languages as $language) {
            if ((utf8_strlen($this->request->post['tltblog_path'][$language['language_id']]) < 3) || (utf8_strlen($this->request->post['tltblog_path'][$language['language_id']]) > 64)) {
                $this->error['tltblog_path'][$language['language_id']] = $this->language->get('error_path');
            }
        }

		if ($this->request->post['tltblog_show_date']) {
			$date = date_create('2000-01-01');

			if (!date_format($date, $this->request->post['tltblog_date_format'])) {
				$this->error['tltblog_date_format'] = $this->language->get('error_date_format');
			}
		}

		$this->load->model('extension/tltblog/url_alias');

        foreach ($this->request->post['tltblog_path'] as $language_id => $value) {
            if (!$this->model_extension_tltblog_url_alias->checkUrlAliasIsFree($value, 'tltpath=' . $value)) {
                $this->error['tltblog_path'][$language_id] = $this->language->get('error_path_exist');
            }
        }

		if ($this->request->post['tltblog_show_path']) {
			foreach ($this->request->post['tltblog_path_title'] as $language_id => $value) {
				if ((utf8_strlen($value['path_title']) < 3) || (utf8_strlen($value['path_title']) > 64)) {
					$this->error['tltblog_path_title'][$language_id] = $this->language->get('error_path_title');
				}
			}
		}

		foreach ($this->request->post['tltblog_meta'] as $language_id => $value) {
			if ((utf8_strlen($value['meta_title']) < 3) || (utf8_strlen($value['meta_title']) > 255)) {
				$this->error['meta_title'][$language_id] = $this->language->get('error_meta_title');
			}
		}

		if ($this->request->post['tltblog_show_image']) {
			if (!$this->request->post['tltblog_width'] || ((int)$this->request->post['tltblog_width'] < 1)) {
				$this->error['tltblog_width'] = $this->language->get('error_width');
			}
	
			if (!$this->request->post['tltblog_height'] || ((int)$this->request->post['tltblog_height'] < 1)) {
				$this->error['tltblog_height'] = $this->language->get('error_height');
			}
		}
		
		if ($this->request->post['tltblog_twitter']) {
			if ((utf8_strlen($this->request->post['tltblog_twitter_name']) < 3) || (utf8_strlen($this->request->post['tltblog_twitter_name']) > 64)) {
				$this->error['twitter_name'] = $this->language->get('error_twitter_name');
			}
			
			if (strncmp($this->request->post['tltblog_twitter_name'], '@', 1)) {
				$this->error['twitter_name'] = $this->language->get('error_twitter_name');
			}
		}
		
		if ($this->request->post['tltblog_facebook']) {
			if ((utf8_strlen($this->request->post['tltblog_facebook_name']) < 3) || (utf8_strlen($this->request->post['tltblog_facebook_name']) > 64)) {
				$this->error['facebook_name'] = $this->language->get('error_facebook_name');
			}
		}
		
		return !$this->error;
	}

    public function install()
    {
        $this->load->model('setting/setting');
        $this->load->model('extension/tltblog/settings');

        $db_version = $this->config->get('tltblog_db_version');
        $edition = $this->config->get('tltblog_edition');
        $check_tables = $this->model_extension_tltblog_settings->checkTables();

        if (!$check_tables) {
            $this->model_extension_tltblog_settings->install();
            $tltblog_settings = array();
        } elseif ($edition !== TLT_BLOG_EDITION) {
            $this->model_extension_tltblog_settings->upgradeFromFree();
            $tltblog_settings = $this->model_extension_tltblog_settings->getSettings();
        } elseif ($db_version !== TLT_BLOG_DB_VERSION) {
            $this->model_extension_tltblog_settings->upgrade();
            $tltblog_settings = $this->model_extension_tltblog_settings->getSettings();
        } elseif ($this->model_extension_tltblog_settings->isPro()) {
            $this->model_extension_tltblog_settings->upgrade();
            $tltblog_settings = $this->model_extension_tltblog_settings->getSettings();
        }

        $tltblog_settings['tltblog_db_version'] = TLT_BLOG_DB_VERSION;
        $tltblog_settings['tltblog_edition'] = TLT_BLOG_EDITION;
        $tltblog_settings['tltblog_version'] = TLT_BLOG_VERSION;
        $tltblog_settings['tltblog_settings_status'] = '0';
        $tltblog_settings['tltblog_tltblog_status'] = isset($tltblog_settings['tltblog_tltblog_status']) ? $tltblog_settings['tltblog_tltblog_status'] : '0';
        $tltblog_settings['tltblog_tlttag_status'] = isset($tltblog_settings['tltblog_tlttag_status']) ? $tltblog_settings['tltblog_tlttag_status'] : '0';

        $this->model_setting_setting->editSetting('tltblog', $tltblog_settings);
    }

    public function uninstall() {
        $this->load->model('setting/setting');
        $this->model_setting_setting->deleteSetting('tltblog');

        if (TLT_BLOG_DROP_DB) {
            $this->load->model('extension/tltblog/settings');
            $this->model_extension_tltblog_settings->uninstall();
        } else {
            $this->model_setting_setting->editSetting('tltblog', [
                'tltblog_db_version'    => TLT_BLOG_DB_VERSION,
                'tltblog_edition'       => TLT_BLOG_EDITION,
                'tltblog_version'       => TLT_BLOG_VERSION
            ]);
        }

        $this->cache->delete('tltblog');
    }

    protected function upgrade()
    {
        $this->load->model('setting/setting');
        $this->load->model('extension/tltblog/settings');

        $tltblog_settings = $this->model_extension_tltblog_settings->getSettings();

        if ($this->config->get('tltblog_version') === '5.1.4') {
            if ($this->config->get('tltblog_status')) {
                $tltblog_settings['tltblog_tltblog_settings_status'] = '1';
                $tltblog_settings['tltblog_tltblog_status'] = '1';
                $tltblog_settings['tltblog_tlttag_status'] = '1';
            } else {
                $tltblog_settings['tltblog_tltblog_settings_status'] = '0';
                $tltblog_settings['tltblog_tltblog_status'] = '0';
                $tltblog_settings['tltblog_tlttag_status'] = '0';
            }
        } else {
            if ($this->config->get('tltblog_status')) {
                $tltblog_settings['tltblog_tltblog_settings_status'] = '1';
                $tltblog_settings['tltblog_tltblog_status'] = '1';
                $tltblog_settings['tltblog_tlttag_status'] = '1';
            } else {
                $tltblog_settings['tltblog_tltblog_settings_status'] = '0';
                $tltblog_settings['tltblog_tltblog_status'] = '0';
                $tltblog_settings['tltblog_tlttag_status'] = '0';
            }
        }

        $tltblog_settings['tltblog_db_version'] = TLT_BLOG_DB_VERSION;
        $tltblog_settings['tltblog_edition'] = TLT_BLOG_EDITION;
        $tltblog_settings['tltblog_version'] = TLT_BLOG_VERSION;

        $this->model_setting_setting->editSetting('tltblog', $tltblog_settings);
    }
}
