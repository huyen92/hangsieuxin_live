<?php
class ControllerExtensionModuleTltBlogDisqus extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/tltblog_disqus');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_tltblog_disqus', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		$language_keys = array(
            'heading_title',
            'text_edit',
            'text_enabled',
            'text_disabled',
            'entry_shortname',
            'entry_status',
            'help_shortname',
            'button_save',
            'button_cancel'
        );

        foreach ($language_keys as $key) {
            $data[$key] = $this->language->get($key);
        }

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/tltblog_disqus', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/tltblog_disqus', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['module_tltblog_disqus_shortname'])) {
			$data['module_tltblog_disqus_shortname'] = $this->request->post['module_tltblog_disqus_shortname'];
		} elseif ($this->config->has('module_tltblog_disqus_shortname')) {
			$data['module_tltblog_disqus_shortname'] = $this->config->get('module_tltblog_disqus_shortname');
		} else {
			$data['module_tltblog_disqus_shortname'] = '';
		}

		if (isset($this->request->post['module_tltblog_disqus_status'])) {
			$data['module_tltblog_disqus_status'] = $this->request->post['module_tltblog_disqus_status'];
		} else {
			$data['module_tltblog_disqus_status'] = $this->config->get('module_tltblog_disqus_status');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/tltblog_disqus', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/tltblog_disqus')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function install()
    {

    }

    public function uninstall()
    {
        $this->load->model('setting/setting');
        $this->model_setting_setting->deleteSetting('module_tltblog_disqus');

		$this->cache->delete('tltblog_disqus');
    }
}