<?php
// HTTP
define('HTTP_SERVER', 'https://hangsieuxin.com/admin/');
define('HTTP_CATALOG', 'https://hangsieuxin.com/');

// HTTPS
define('HTTPS_SERVER', 'https://hangsieuxin.com/admin/');
define('HTTPS_CATALOG', 'https://hangsieuxin.com/');

// DIR
define('DIR_APPLICATION', '/var/www/vhosts/hangsieuxin.com/httpdocs/admin/');
define('DIR_SYSTEM', '/var/www/vhosts/hangsieuxin.com/httpdocs/system/');
define('DIR_IMAGE', '/var/www/vhosts/hangsieuxin.com/httpdocs/image/');
define('DIR_STORAGE', '/var/www/vhosts/hangsieuxin.com/storage/');
define('DIR_CATALOG', '/var/www/vhosts/hangsieuxin.com/httpdocs/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'han29623_hangsieuxin');
define('DB_PASSWORD', 'JiJi%7BiDe#9PiJu');
define('DB_DATABASE', 'han29623_opencart');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');