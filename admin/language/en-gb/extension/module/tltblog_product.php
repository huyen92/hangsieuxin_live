<?php
// Heading
$_['heading_title']    	= 'TLT Blog - Product Module';

// Text
$_['text_module']      	= 'Modules';
$_['text_success']     	= 'Success: You have modified TLT Blog - Product Module!';
$_['text_edit']        	= 'Edit TLT Blog - Product Module';
$_['text_sortorder']    = 'By Sort Order';
$_['text_abc']      	= 'Alphabetically';
$_['text_help']			= 'This module shows related articles on a product details page. Do not forget to add this module to the Product layout!';

// Entry
$_['entry_name']        = 'Module Name';
$_['entry_title']      	= 'Module Title';
$_['entry_show_title']	= 'Show Module Title';
$_['entry_sort']      	= 'Sort Order';
$_['entry_show_image']	= 'Show Image';
$_['entry_width']      	= 'Width';
$_['entry_height']     	= 'Height';
$_['entry_status']     	= 'Status';

// Help

// Error
$_['error_permission'] 	= 'Warning: You do not have permission to modify TLT Blog - Product Module!';
$_['error_name']        = 'Module Name must be between 3 and 64 characters!';
$_['error_title']       = 'Module Title must be between 3 and 64 characters!';
$_['error_width']      	= 'Width required!';
$_['error_height']     	= 'Height required!';
