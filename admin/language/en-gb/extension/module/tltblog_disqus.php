<?php
// Heading
$_['heading_title']    = 'TLT Blog - Disqus Plugin';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified TLT Blog - Disqus Plugin!';
$_['text_edit']        = 'Edit TLT Blog - Disqus Plugin';

// Entry
$_['entry_shortname']  = 'Disqus Shortname';
$_['entry_status']     = 'Status';

// Help
$_['help_shortname']   = 'The uniquely identifier of your website on Disqus.';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify TLT Blog - Disqus Plugin!';