<?php
class ModelExtensionTltBlogPlugins extends Model {
	public function getTltBlogsForProduct($product_id, $sort = 1, $show_in_sitemap = true) {
	    $blogs = $this->cache->get('tltblog.product.' . (int)$product_id . '.' . (int)$sort . '.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'));
		
		if (!$blogs) {
			if ($sort == '1') {
				$orderby = 'ORDER BY b.sort_order, LCASE(bd.title) ASC';
			} else {
				$orderby = 'ORDER BY LCASE(bd.title) ASC';
			}
			
			$query = $this->db->query("SELECT DISTINCT br.tltblog_id, b.image, bd.title, bd.intro FROM " . DB_PREFIX . "tltblog_related br LEFT JOIN " . DB_PREFIX . "tltblog b ON (br.tltblog_id = b.tltblog_id) LEFT JOIN " . DB_PREFIX . "tltblog_description bd ON (b.tltblog_id = bd.tltblog_id) LEFT JOIN " . DB_PREFIX . "tltblog_to_store b2s ON (b.tltblog_id = b2s.tltblog_id) WHERE br.related_id = '" . (int)$product_id . "' AND b.status = '1' AND b2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND bd.language_id = '" . (int)$this->config->get('config_language_id') . "'" . " " . $orderby);

			$blogs = $query->rows;

			$this->cache->set('tltblog.product.' . (int)$product_id .'.' . (int)$sort . '.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'), $blogs);
		}

		return $blogs;
	}
}
