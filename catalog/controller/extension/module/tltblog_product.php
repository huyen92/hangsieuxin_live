<?php

class ControllerExtensionModuleTltBlogProduct extends Controller {
	public function index($setting) {
		if (isset($setting['module_description'][$this->config->get('config_language_id')]['title'])) {
		    $debugOn = false;

			$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
			$data['show_title'] = $setting['show_title'];
			
			$data['show_image'] = $setting['show_image'];
			
			$this->load->model('extension/tltblog/plugins');
			$this->load->model('setting/setting');
			$this->load->model('tool/image');

			if ($debugOn) {
			    $this->log->write('TLT Blog: models loaded.');
            }

			if ($this->config->get('tltblog_seo')) {
				require_once(DIR_APPLICATION . 'controller/extension/tltblog/tltblog_seo.php');
				$tltblog_seo = new ControllerExtensionTltBlogTltBlogSeo($this->registry);
				$this->url->addRewrite($tltblog_seo);
			}

            if ($debugOn) {
                $this->log->write('TLT Blog: SEO controller attached.');
            }

            $data['tltblogs'] = array();

			if (isset($setting['tags_to_show'])) {
				$where_tags = $setting['tags_to_show'];
			} else {
				$where_tags = array();
			}

            $results = array();

            if (isset($this->request->get['product_id'])) {
                if ($debugOn) {
                    $this->log->write('TLT Blog: product_id = ' . $this->request->get['product_id']);
                }

                $results = $this->model_extension_tltblog_plugins->getTltBlogsForProduct($this->request->get['product_id'], $setting['sort']);
            } else {
			    $this->log->write('TLT Blog: somebody steeled product_id from request');
            }

            if ($debugOn) {
                $this->log->write('TLT Blog: related posts loaded.');
            }

            if ($results) {
                if ($this->config->has('tltblog_path')) {
                    $path_array = $this->config->get('tltblog_path');
                }

				if (isset($path_array[$this->config->get('config_language_id')])) {
					$path = $path_array[$this->config->get('config_language_id')];
				} else {
					$path = 'blogs';
				}
								
				foreach ($results as $result) {
					if ($result['image']) {
						$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}
					
					$data['tltblogs'][] = array(
						'tltblog_id'  		=> $result['tltblog_id'],
						'thumb'       		=> $image,
						'title'       		=> $result['title'],
						'intro'       		=> html_entity_decode($result['intro'], ENT_QUOTES, 'UTF-8'),
						'href'        		=> $this->url->link('extension/tltblog/tltblog', 'tltpath=' . $path . '&tltblog_id=' . $result['tltblog_id'])
					);
				}

                if ($debugOn) {
                    $this->log->write('TLT Blog: everything ok.');
                }

                return $this->load->view('extension/module/tltblog_product', $data);
			}
		}
	}
}