<?php
class ControllerExtensionModuleTltBlogDisqus extends Controller {
	public function index() {
		$this->load->model('setting/setting');

        if (strcmp($this->request->get['route'], 'extension/tltblog/tltblog') === 0) {
            $data['page_identifier'] = $this->request->get['tltblog_id'];
        } else {
            return;
        }

        if ($this->config->has('module_tltblog_disqus_shortname')) {
            $data['disqus_shortname'] = $this->config->get('module_tltblog_disqus_shortname');
        } else {
            return;
        }

        if (isset($this->request->server['HTTPS'])) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        if (isset($this->request->get['_route_'])) {
            $data['page_url'] = $server . $this->request->get['_route_'];
        } else {
            $data['page_url'] = '';
        }

        $data['page_title'] = $this->document->getTitle();

        $data['language'] = mb_substr($this->session->data['language'], 0, 2);

		return $this->load->view('extension/module/tltblog_disqus', $data);
	}
}