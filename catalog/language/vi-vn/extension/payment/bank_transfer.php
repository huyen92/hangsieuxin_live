<?php
// Text
$_['text_title']       = 'Chuyển khoản';
$_['text_instruction'] = 'Hướng dẫn chuyển khoản';
$_['text_description'] = 'Khách hàng có thể chuyển khoản cho chúng tôi qua tài khoản: ';
$_['text_payment']     = 'Đơn đặt hàng sẽ không được ship khi khách hàng chưa hoàn tất việc chuyển khoản';